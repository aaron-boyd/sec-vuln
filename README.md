# Security Vulnerability Assessment

[Course Website](http://gauss.ececs.uc.edu/Courses/c5156/)

## Learning Objectives
### Knowledge and Comprehension
* How to disclose a security vulnerability in an ethical fashion
* Vulnerability reporting policy considerations
* Legal doctrines affecting vulnerability reporting
* A variety of software, and OS vulnerabilities
* Network and protocol vulnerabilities
* Various side-channel attacks and their effectiveness, plus countermeasures
* Human factors that affect vulnerability
* Password and key management
* Examples of fraud in E-commerce
* Vulnerabilities in Digital Rights Management technologies

### Application
* Remove race conditions from code that has at least one
* Analyze a KDC protocol for attacks
* Determine whether a piece of code has a buffer overflow vulnerability
* Determine an exponent from a given exponentiation algorithm implementation,
  given timing data
* Recover at least one key bit from a smart card by inducing faults
* Design a Return Oriented Programming attack on code that is vulnerable to
  buffer overflow 
