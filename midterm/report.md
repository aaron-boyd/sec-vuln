# Security Vulnerability Assessment - Midterm
Aaron Boyd

## Assignment
Develop a stealth communication mechanism for sending messages between
computers. It should be impossible or at least extremely difficult for a system
administrator to check whether such messages are actually communicated - either
while a message is being sent or that a message had been sent earlier. It is OK
to use steganography or IP headers to hide messages but:

1. I will be impressed with another approach that has not reached popularity
2. I do not care what the messages actually are - I only care about detecting
   the existence of a message - so checking header values and traffic levels
   should create a challenge for both those approaches
3. I will be really impressed if you use an OS or application vulnerability to
   create the covert channel.

## NTP Packet Structure
```c
typedef struct
{
    uint8_t li_vn_mode; // Eight bits. li, vn, and mode.
                        // li.   Two bits.   Leap indicator
                        // vn.   Three bits. Version number of the protocol
                        // mode. Three bits. Client will pick mode 3 for client

    uint8_t stratum;   // Stratum level of the local clock.
    uint8_t poll;      // Maximum interval between successive messages.
    uint8_t precision; // Precision of the local clock.

    uint32_t rootDelay;      // Total round trip delay time.
    uint32_t rootDispersion; // Max error aloud from primary clock source.
    uint32_t refId;          // Reference clock identifier.

    uint32_t refTm_s; // Reference time-stamp seconds.
    uint32_t refTm_f; // Reference time-stamp fraction of a second.

    uint32_t origTm_s; // Originate time-stamp seconds.
    uint32_t origTm_f; // Originate time-stamp fraction of a second.

    uint32_t rxTm_s; // Received time-stamp seconds.
    uint32_t rxTm_f; // Received time-stamp fraction of a second.

    uint32_t txTm_s; // 32 bits and the most important field the client cares
                     // about. Transmit time-stamp seconds.
    uint32_t txTm_f; // 32 bits. Transmit time-stamp fraction of a second.

} ntp_packet; // Total: 384 bits or 48 bytes.
```

## Protocol
The covert NTP channel protocol uses two 32-bit fields to hide message
information. The first 32-bit field contains control data and the second
contains any data used by the control field.

A single covert NTP packet looks like the following:
```c
struct Packet
{
    uint32_t control;
    uint32_t data;
};
```

## NTP Timestamp Structure
The NTP protocol keeps track of timestamps using two 32-bit fields:
1. Timestamp Seconds
2. Timestamp Seconds Fraction

### Client
The covert NTP channel client uses the "Transmit Timestamp" field of the NTP
protocol to hide its messages.

This choice was made through observing an `openntpd` daemon in Wirehark, it can
be seen when a client sends query to a server that it chooses a random value for
the "Transmit Timestamp". Therefore, one can hide data in this field without
arousing suspicion from a system administrator.

![OpenNTPd Client Query in Wireshark](./images/client-query.png)

### Server
The covert NTP channel server uses the "Origin Timestamp" field of the NTP
protocol to hide its messages.

This choice was made for a similar reason as the client. In Wireshark it can be
seen that when messages come in from an NTP server that the Origin Timestamp
field contains a random value. Therefore, data can be hidden in this field.

![NTP Server Response in Wireshark](./images/server-response.png)

## Control Data
The control field of the covert NTP protocol uses the following values:

| Control Name  | Value | Description                        | Data Description |
| ------------- | ----- | ---------------------------------- | ---------------- |
| ACK_SIZE      | 1     | Acknowledge message size           |                  |
| COMPLETE      | 2     | Message transfer complete          |                  |
| DATA          | 3     | Packet contains data               | data             |
| MESSAGE_CHECK | 4     | Check server for messages          |                  |
| MESSAGE_SIZE  | 5     | Size of message about to be sent   | int              |
| RECEIVE_ACK   | 6     | Acknowledge data received          |                  |
| RECEIVE_ERROR | 7     | Indicate receive error             |                  |
| RECEIVE_READY | 8     | Indicate ready to receive messages |                  |
| RESET         | 9     | Drop connection for any reason     |                  |
| SEND_ERROR    | 10    | Indicate send error                |                  |
| SEND_READY    | 11    | Indicate ready to send message     |                  |

## Sending Messages
Since NTP connections are only initiated from the client there are two cases
in which messages will be exchanged:
1. Client tells the server it wants to send a message
2. Client tells the server it wants to receive a message

### Client Sending Message to Server
```
Client            Server
+----------------------+
|     SEND_READY       |
|--------------------->|
|     RECEIVE_READY    |
|<---------------------|
|     MESSAGE_SIZE     |
|--------------------->|
|     ACK_SIZE         |
|<---------------------|
|     DATA             |
|--------------------->|
|     RECEIVE_ACK      |
|<---------------------|
|                      |
|     ...              |
|                      |
|     DATA             |
|--------------------->|
|     COMPLETE         |
|<---------------------|
V                      V
```

### Covert NTP Channel Notes## Client Receiving Message from Server
```
Client            Server
+----------------------+
|     MESSAGE_CHECK    |
|--------------------->|
|     SEND_READY       |
|<---------------------|
|     RECEIVE_READY    |
|--------------------->|
|     MESSAGE_SIZE     |
|<---------------------|
|     ACK_SIZE         |
|--------------------->|
|     DATA             |
|<---------------------|
|     RECEIVE_ACK      |
|--------------------->|
|                      |
|     ...              |
|                      |
|     DATA             |
|<---------------------|
|     COMPLETE         |
|--------------------->|
V                      V
```
