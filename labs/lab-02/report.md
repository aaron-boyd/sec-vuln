# Security Vulnerability Assessment - Lab 2
Aaron Boyd

## Assignment
1. Analyzing the old & new code, and identifying memory corruption
   vulnerabilities
2. Making recommendations for how to rewrite the program so it is not
   vulnerable
3. Identify the nature of the memory corruption, describing (or diagraming)
   how it corrupts memory in the application space (possibly use GDB or another
   tool)
4. Write a report which documents these findings and also documents your
   proposed solution
5. You must solve the bug while using -fno-stack-protector for GCC, as the new
   OS still doesn't support this feature
6. The solution must result in normal, non-crashing, program flow

## Notes

### Compiler Flags
`fno-stack-protector`
: Don't use stack-canary

`g2`
: Include debug info of level 2

`O0`
: Reduce compilation time and make debugging produce the expected results.
  This is the default.

### Disclaimer
Some stack addresses change throughout this report. This is a result of
running the application multiple times and gdb using different memory
locations for the stack each time. The application, however, remained
unchanged for all screenshots in this report.

## Memory Corruption Vulnerability
### Static Analysis
g++ warns that the value `0x4040404040404040404040404040` (14 bytes) is too
large for type `unsigned long long` (8 bytes).

```
$ make
g++ -c -o expl1.o -fno-stack-protector -g2 -O0 expl1.cpp
expl1.cpp:8:29: warning: integer constant is too large for its type
    ABaseClass a = ANewClass(0x4040404040404040404040404040);
                             ^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
g++ -c -o ABaseClass.o -fno-stack-protector -g2 -O0 ABaseClass.cpp
g++ -c -o ANewClass.o -fno-stack-protector -g2 -O0 ANewClass.cpp
g++ -o expl1 -fno-stack-protector -g2 -O0 expl1.o ABaseClass.o ANewClass.o
```

The compiler truncates this to `0x4040404040404040` (8 bytes) so that is okay
(address `main<+19>`).

```
0x0000555555555145 <+0>:	push   rbp
0x0000555555555146 <+1>:	mov    rbp,rsp
0x0000555555555149 <+4>:	sub    rsp,0x30
0x000055555555514d <+8>:	mov    DWORD PTR [rbp-0x24],edi
0x0000555555555150 <+11>:	mov    QWORD PTR [rbp-0x30],rsi
0x0000555555555154 <+15>:	lea    rax,[rbp-0x10]
0x0000555555555158 <+19>:	movabs rsi,0x4040404040404040
0x0000555555555162 <+29>:	mov    rdi,rax
0x0000555555555165 <+32>:	call   0x55555555523c <ANewClass::ANewClass(unsigned long long)>
0x000055555555516a <+37>:	lea    rdx,[rbp-0x10]
0x000055555555516e <+41>:	lea    rax,[rbp-0x20]
0x0000555555555172 <+45>:	mov    rsi,rdx
0x0000555555555175 <+48>:	mov    rdi,rax
0x0000555555555178 <+51>:	call   0x555555555190 <ABaseClass::ABaseClass(ABaseClass&&)>
0x000055555555517d <+56>:	lea    rax,[rbp-0x20]
0x0000555555555181 <+60>:	mov    rdi,rax
0x0000555555555184 <+63>:	call   0x55555555520a <ABaseClass::show_data()>
0x0000555555555189 <+68>:	mov    eax,0x0
0x000055555555518e <+73>:	leave
0x000055555555518f <+74>:	ret
```

---

### Control Flow
```cpp
main()
    |-> ANewClass::ANewClass(unsigned long long input);
    |      |-> ABaseClass::ABaseClass(unsigned short input);
    |      |      \-> sprintf(this->numbuf, "%hu", input);
    |      \-> sprintf(this->get_buf(), "%llu", input);
    \-> ABaseClass::show_data();
           \-> printf("%s\n", this->numbuf);
```

The use of `sprintf` on a char[6] immediately stood out to me as a potential
vulnerability. This is because C++ does not check bounds when working with
arrays and `sprintf` does not indicate or guarantee a certain number of
characters/bytes that will be written to the array.

The fact that `ANewClass` uses an `unsigned long long` which is 8 bytes made
me more suspicious of `sprintf` on a `numbuf` char[6]. A 6-byte string needs
a `\0` character at the end so that leaves room for 5 characters. "99999"
would be the largest value we could safely write to this array. 99,999 in hex
is `0x01869f` which is 3 bytes. As `unsigned long long` is 8 bytes so it is
likely numbers larger than 99,999 will be passed into `ANewClass` which
raises a red flag for a potential buffer overflow.

---

### Dynamic Analysis
From static analysis, I hypothesized that `sprintf` into `numbuf` was
vulnerable to a buffer overflow. To view how `numbuf` is manipulated at
runtime I used gdb with the [gef](https://github.com/hugsy/gef) feature set.

When the `a` object of type `ANewClass` is created in `main`. The `ANewClass`
constructor is called. The `ANewClass` constructor immediately passes `input`
to the `ABaseClass` constructor and casts it from an `unsigned long long`
(8 bytes) to an `unsigned short` (2 bytes). As a result, the first 6 bytes of
`0x4040404040404040` are "chopped off" leaving `0x4040`. When the `ABaseClass`
constructor calls `sprintf` the string representation of `0x4040` is "16448" or
`0x313634343800`. This string is 6 bytes (including the null-terminating byte)
long which luckily fits perfectly into `numbuf` char[6].

Figure 1 shows the `a` object starts at `0x7fffffffe170`. In the red rectangle
is a pointer to the objects virtual function table and the green rectangle
contains `numbuf` char[6].

![Figure 1: `ABaseClass` constructor after `sprintf` in GDB](./images/ABaseClass_post_printf.png "Figure 1: `ABaseClass` constructor after `sprintf` in GDB")

<center>(Figure 1: `ABaseClass` constructor after `sprintf` in GDB)</center>

The `ANewClass` constructor calls `sprintf` into `numbuf`. The string
representation of `0x4040404040404040` is "4629771061636907072" which is 20
bytes (including the null-terminating byte). As C++ does not do bounds checking
the full 20 bytes are written to memory from `0x7fffffffe178` to
`0x7fffffffe18B`. This is well out of bounds of `numbuf`'s 6 bytes. In Figure
2, the `a` object's virtual function table is in the red rectangle,
`numbuf` is in green, and the memory that `sprintf` wrote to is in blue.

![Figure 2: `ANewClass` constructor after `sprintf` in GDB](./images/ANewClass_post_printf.png "Figure 2: `ANewClass` constructor after `sprintf` in GDB")

<center>(Figure 2: `ANewClass` constructor after `sprintf` in GDB)</center>

Back in `main`, once the `ANewClass` constructor finishes, the object is cast
to `ABaseClass`. This explains why two objects are allocated on the stack.
This is not inherently obvious looking at the source code:

```cpp
ABaseClass a = ANewClass(0x4040404040404040404040404040);
```

However, it is important however to remember in C++ the `=` operator can be
treated as a function like:

```cpp
ABaseClass a;
a.operator=(ANewClass(0x4040404040404040404040404040));
```

This makes the existence of two objects on the stack a little more obvious as
there needs to be a `ABaseClass` to call `operator=()` and the `ANewClass`
being passed in.

These two objects can be seen in Figure 3. The `ANewClass` object starts at
`0x7fffffffe0e0` and the `ABaseClass` object starts at `0x7fffffffe0d0`.

![ABaseClass and ANewClass objects main() stack view](
./images/main_two_objects_on_stack.png "ABaseClass and ANewClass objects main()
stack view")

<center>(Figure 3: ABaseClass and ANewClass objects main() stack view)</center>

The existence of two objects can also be seen by looking at the disassembly of
`main` where the `ANewClass::ANewClass(unsigned long long)` function is called
and then that object is passed into the `ABaseClass::ABaseClass(ABaseClass&&)`
function. This is nothing of concern. I just thought it was interesting to see
how g++ handled casting objects because I was very confused why I saw the
string "462977" twice on the stack (see Figure 3).

```
0x0000555555555165 <+32>:	call   0x55555555523c <ANewClass::ANewClass(unsigned long long)>
0x000055555555516a <+37>:	lea    rdx,[rbp-0x10]
0x000055555555516e <+41>:	lea    rax,[rbp-0x20]
0x0000555555555172 <+45>:	mov    rsi,rdx
0x0000555555555175 <+48>:	mov    rdi,rax
0x0000555555555178 <+51>:	call   0x555555555190 <ABaseClass::ABaseClass(ABaseClass&&)>
```

The memory corruption caused by the `ANewClass` object is also
visible in Figure 3. The `ANewClass` object has overwritten the memory pointed
to by `rbp` and 4 bytes of the return address. The value pointed to by `rbp`
is unfortunately the base address of the stack frame used by the function that
called `main`; in this case `__libc_start_main`. This can be seen in gdb at the
start of `main` before the stack is corrupted (see Figure 4).

![main function's clean stack](./images/main_old_rbp_ret.png "main function's
clean stack")

<center>(Figure 4: main function's clean stack)</center>

The call to `a.show_data()` is semi-successful and prints out "462977". As
mentioned earlier, there are two objects in `main`'s stack frame: `ABaseClass`
and `ANewClass`. In Figure 3, it can be seen that after the objects are
populated, the `ABaseClass::numbuf` at `0x7fffffffe0d8` is "46977" and
`ANewClass::numbuf` at `0x7fffffffe0e8` is "4629771061636907072". This is a
result of the 6 bytes allocated for `ANewClass::numbuf` being copied into
`ABaseClass::numbuf` when the `ANewClass` object is passed to
`ABaseClass::ABaseClass(ABaseClass&&)`. Luckily the memory that was allocated
had zeroes after those 6 bytes and was able to be safely printed by `printf`.
It
was successful because it printed the data but failed because it got lucky.

---

When `main` returns to `__libc_start_main` it executes the `leave` and `ret`
instructions which can be translated to:

```
mov rsp,rbp
pop rbp
pop rip
```

These instructions are successful because they are just moving values and
reading values off the stack. The program, however, fails once the processor
tries to read the instruction at the memory address now pointed to by `rip`,
`0x7fff00323730`. This causes a segmentation fault because this is not a valid
memory address (see Figure 5).

![Segmentation fault on exit of main](./images/seg_fault.png "Segmentation fault
on exit of main")

<center>(Figure 5: Segmentation fault on exit of main)</center>

---

### Why is this a problem?
This a problem not only because it causes stack-corruption and a segmentation
fault but if a user was able to control the value of `input` they could
perform a return-oriented programming (ROP) attack. If the user was able to
get the address of another function that they wanted to call, they could
manipulate `input` so it modifies the return address to be that value instead
of a garbage value. This would cause `main()` to return to that function
instead of `__libc_start_main`.

---

## Recommendations
My recommendation would be to increase the size of `numbuf` in `ABaseClass`
from 6 to 20. The largest usable primitive in C++ on a 64-bit platform is 8
bytes. 8 bytes is 64 bits. (2 ^ 64) - 1 = 18446744073709551615.
18446744073709551615 is 19 characters. Add one more for a `\0` character. This
results in 20 characters as the maximum length string these classes will ever
produce using C++ primitives. This will prevent a buffer overflow from
occurring.

In my changed code, I changed the value `0x4040404040404040404040404040` to
`0x4040404040404040` because this value is too big for C++ to handle and the
compiler already converts it to `0x4040404040404040` on a 64-bit architecture.
Next, I added a copy-constructor to the `ABaseClass`:

```cpp
ABaseClass &ABaseClass::operator=(const ABaseClass &other)
{
    // convert other's char[] to string
    auto str = std::string(other.numbuf);
    // convert string to unsigned short
    auto num = static_cast<unsigned short>(std::stoull(str));
    // print num as unsigned short into numbuf
    sprintf(this->numbuf, "%hu", num);
    return *this;
}
```

to handle code like:

```cpp
ANewClass a(0x4040404040404040);
ABaseClass b;
b = a;
b.show_data(); // prints "16448" instead of "4629771061636907072"
```

I also added an overload for the `operator ABaseClass()` in the `ANewClass`
class.

```cpp
ANewClass::operator ABaseClass()
{
    // convert char[] to string
    auto str = std::string(this->numbuf);
    // convert string to unsigned short
    auto num = static_cast<unsigned short>(std::stoull(str));
    // return new ABaseClass object
    return ABaseClass(num);
}
```

This was an attempt to get the following code to work properly.

```cpp
ABaseClass a = ANewClass(0x4040404040404040);
```

This overload allowed me to do the following:

```cpp
ABaseClass a = ANewClass(0x4040404040404040).operator ABaseClass();
```

I found this was the best way to avoid using pointers like the existing code.

I am opposed to the existing code's lack of pointers or references. I could
not find any documentation that supported this kind of type conversion because
code like this should not really exist. Increasing `numbuf`'s size to 20
creates a secure application but it won't behave as one would expect. The
existing code takes a `ANewClass` object and essentially copies it's `numbuf`
into a new `ABaseClass` object's `numbuf`. This is incorrect because `numbuf`
is the string representation of an `unsigned long long` and not an
`unsigned short` like `ABaseClass` wants.

`(unsigned short)0x4040404040404040 = 0x4040 = 16448`

To avoid this, I would use pointers like:

```cpp
ABaseClass* a = new ANewClass(0x4040404040404040);
a->show_data(); // prints 4629771061636907072 because underlying object is ANewClass
```

The underlying object is still a `ANewClass` object and is not being
implicitly converted incorrectly to a `ABaseClass` object. You could also make
the `show_data` function `virtual` and have each class convert `numbuf` to the
correct primitive type every time it is called.
