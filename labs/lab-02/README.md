# Lab 2 - Characterize a memory corruption vulnerability

Due: 02/08/2021

[Assignment](http://gauss.ececs.uc.edu/Courses/c5156/labs/lab2-memcorrupt.html)

[Report](./report.md)