#include "include/ABaseClass.h"

ABaseClass::ABaseClass()
{
    memset(this->numbuf, 0, this->size);
}

ABaseClass::ABaseClass(unsigned short input)
{
    memset(this->numbuf, 0, this->size);
    sprintf(this->numbuf, "%llu", input);
}

void ABaseClass::show_data(void)
{
    printf("%s\n", this->numbuf);
}

char *ABaseClass::get_buf(void)
{
    return this->numbuf;
}

ABaseClass &ABaseClass::operator=(const ABaseClass &other)
{
    // convert other's char[] to string
    auto str = std::string(other.numbuf);
    // convert string to unsigned short
    auto num = static_cast<unsigned short>(std::stoull(str));
    // print num as unsigned short into numbuf
    sprintf(this->numbuf, "%hu", num);
    return *this;
}