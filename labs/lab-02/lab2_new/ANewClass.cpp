#include "include/ANewClass.h"

ANewClass::ANewClass(unsigned long long input) : ABaseClass((unsigned short)input)
{
    memset(this->get_buf(), 0, this->size);
    sprintf(this->get_buf(), "%llu", input);
}

ANewClass::operator ABaseClass()
{
    // convert char[] to string
    auto str = std::string(this->numbuf);
    // convert string to unsigned short
    auto num = static_cast<unsigned short>(std::stoull(str));
    // return new ABaseClass object
    return ABaseClass(num);
}