#ifndef _C6056_ABASECLASS_H_
#define _C6056_ABASECLASS_H_

#include <cstdio>
#include <string>
#include <cstring>

/*
8-byte is largest number to expect
(2 ^ (8 * 8)) - 1 = 18446744073709551615
18446744073709551615\0 = 20 bytes
*/
#define MAX_SIZE 20

class ABaseClass
{

public:
    ABaseClass();
    ABaseClass(unsigned short);
    ABaseClass& operator=(const ABaseClass& other);
    virtual void show_data(void);
    virtual char *get_buf(void);

protected:
    size_t size = MAX_SIZE;
    char numbuf[MAX_SIZE];
};

#endif /* _C6056_ABASECLASS_H_ */
