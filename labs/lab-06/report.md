# Security Vulnerability Assessment - Lab 5
Aaron Boyd

## Assignment
The right margin of the course webpage contains 6 images with the following
filenames: erc2.jpg, mainstreet.jpg, erdos.jpg, nit3-3.jpg, loncampus.png, and
bigO.jpg. One of those image files does not have a hidden message. The other 5
images do. Your task is to determine which file has no hidden message and the
hidden message in each of the other files. All hidden messages are short: no
more than a sentence long. You can use any tool you wish to find the messages
and to find the file with none.

## erc2.jpg
### Message
This was an easy one - message is Stew in your own juice

### Procedure
Run `strings -e b erc2.jpg`

## erdos.jpg
### Message
Dear Friend ; Thank-you for your interest in our publication
. If you no longer wish to receive our publications
simply reply with a Subject: of "REMOVE" and you will
immediately be removed from our club ! This mail is
being sent in compliance with Senate bill 1627 ; Title
6 , Section 303 ! This is NOT unsolicited bulk mail
. Why work for somebody else when you can become rich
as few as 97 WEEKS . Have you ever noticed how many
people you know are on the Internet and more people
than ever are surfing the web ! Well, now is your chance
to capitalize on this . WE will help YOU decrease perceived
waiting time by 130% and process your orders within
seconds . You can begin at absolutely no cost to you
. But don't believe us ! Mr Simpson who resides in
Delaware tried us and says "I was skeptical but it
worked for me" ! We are a BBB member in good standing
! You will blame yourself forever if you don't order
now ! Sign up a friend and your friend will be rich
too . God Bless ! Dear Cybercitizen ; We know you are
interested in receiving hot intelligence ! We will
comply with all removal requests . This mail is being
sent in compliance with Senate bill 2016 ; Title 2
; Section 303 . This is different than anything else
you've seen ! Why work for somebody else when you can
become rich within 39 weeks . Have you ever noticed
most everyone has a cellphone plus more people than
ever are surfing the web . Well, now is your chance
to capitalize on this ! WE will help YOU sell more
plus deliver goods right to the customer's doorstep
! You can begin at absolutely no cost to you ! But
don't believe us ! Mr Simpson of Virginia tried us
and says "Now I'm rich, Rich, RICH" ! This offer is
100% legal ! We BESEECH you - act now . Sign up a friend
and your friend will be rich too ! Thank-you for your
serious consideration of our offer .

### Procedure
Run `strings -e l erdos.jpg`

## bigO.jpg
### Message
tux.jpg image file

![tux.jpg](./images/solved/tux.jpg)

### Procedure
Extract tux.jpg with:
```
binwalk -e bigO.jpg
```

## mainstreet.jpg
### Message
This one may be a little harder - the message is "no soap radio"

### Procedure
Run:
```
steghide extract -sf mainstreet.jpg
```
With no passphrase.

