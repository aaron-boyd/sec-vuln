import base64
import argparse
from PIL import Image


def main():
    parser = argparse.ArgumentParser(description='Get LSB of image')
    parser.add_argument('image',
                        help='Image to use')

    args = parser.parse_args()
    img = Image.open(args.image)

    extracted = ''

    pixels = img.load()

    for col in range(img.width):
        for row in range(img.height):
            r,g,b = pixels[col, row]
            extracted += bin(r)[-1]
            extracted += bin(g)[-1]
            extracted += bin(b)[-1]

    byte_map = {}

    for i in range(0,len(extracted)-8,8):
        bits = extracted[i:i+8]
        num = 0
        for bit in bits:
            num = (num << 1) | int(bit)

        if num in byte_map.keys():
            byte_map[num] += 1
        else:
            byte_map[num] = 1


    print(byte_map)

    lsbs = []
    bit_field = 0b00000001
    with open(args.image, 'rb') as fh:
        img_bytes = fh.read()
        lsbs = [img_byte & bit_field for img_byte in img_bytes]

    decoded = []
    for i in range(0, len(lsbs), 8):
        lsb = lsbs[i:i + 8]
        num = 0
        for b in lsb:
            num = (num << 1) | b

        decoded.append(num)


if __name__=='__main__':
    main()
