#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/* sb is always 2 (reflects 0 and 1)
   sk is the number of bits of the exponent
   si is the number of samples */
int sb = 2, si;
extern int sk;

/* Time it takes for the kth iteration of the exponentiation algorithm
   on input sample i if the kth exponent bit (keybit) is b.

   Suppose i input samples (to be exponentiated) are known.  Then experiments
   can be run to determine how many nanoseconds it takes to perform
   iteration k of the exponentiation algorithm on sample i.  There are two
   experiments per iteration: the keybit is 0 and the keybit is 1.

   Example use:
     get the time is takes to compute the 17th iteration of y^x mod n
     if the 17th exponent bit is 0 and y is input sample number 45:  
        prompt> int tme = getTime(45, 17, 0);  
*/
int getIterationTime(int i, int k, int b);

int getSpyTime(int i, int k);

int main(int argc, char **argv)
{
    int i, k, b;
    if (argc == 1)
    {
        leave(argv[0]);
    }

    si = atoi(argv[1]);

    if (si <= 0)
    {
        leave(argv[0]);
    }

    setup();

    /* ------ above this line is fixed, user additions are below ------*/
    printf("Number of keybits: %d\tNumber of samples: %d\n", sk, si);

    double sampleZeroTimeDiffs[si][sk];
    double sampleOneTimeDiffs[si][sk];

    double zeroTimeSums[sk];
    double oneTimeSums[sk];

    double zeroTimeAverages[sk];
    double oneTimeAverages[sk];

    int keyBits[sk];

    for (i = 0; i < si; i++) // number of samples
    {
        int spyTime = 0;

        // for this sample, get iteration time for each bit in key
        for (k = 0; k < sk; k++)
        {
            spyTime = getSpyTime(i, k);
            sampleZeroTimeDiffs[i][k] = abs(getIterationTime(i, k, 0) \
                                            - spyTime);
            sampleOneTimeDiffs[i][k] = abs(getIterationTime(i, k, 1) \
                                           - spyTime);
        }
    }

    for (k = 0; k < sk; k++) // zero out sum arrays
    {
        zeroTimeSums[k] = 0;
        oneTimeSums[k] = 0;
    }

    // for each bit, calculate average time of samples for that bit
    for (k = 0; k < sk; k++)
    {
        for (i = 0; i < si; i++) // number of samples
        {
            zeroTimeSums[k] += sampleZeroTimeDiffs[i][k];
            oneTimeSums[k] += sampleOneTimeDiffs[i][k];
        }

        // average time for kth bit across all samples
        zeroTimeAverages[k] = zeroTimeSums[k] / si;
        oneTimeAverages[k] = oneTimeSums[k] / si;
    }

    for (k = 0; k < sk; k++) // for each bit, calculate variance
    {
        double zeroSum = 0.0;
        double oneSum = 0.0;
        for (i = 0; i < si; i++) // number of samples
        {
            zeroSum += pow(sampleZeroTimeDiffs[i][k] \
                           - zeroTimeAverages[k], 2.0);
            oneSum += pow(sampleOneTimeDiffs[i][k] \
                          - oneTimeAverages[k], 2.0);
        }

        double zeroBitVariance = zeroSum / (si - 1);
        double oneBitVariance = oneSum / (si - 1);

        printf("%2dth bit: 0=%.5f\t1=%.5f\n", k, zeroBitVariance, oneBitVariance);

        if (zeroBitVariance < oneBitVariance)
        {
            keyBits[k] = 0;
        }
        else
        {
            keyBits[k] = 1;
        }
    }

    long key = 0;
    printf("\nBinary Key: 0b");
    for (k = sk - 1; k >= 0; k--)
    {
        printf("%d", keyBits[k]);
        key += keyBits[k] * pow(2, k);
    }

    printf("\nHex Key: 0x%X\n", key);
}
