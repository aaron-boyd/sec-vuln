# Security Vulnerability Assessment - Lab 5
Aaron Boyd

## Assignment
Your assignment is to modify lab5.c to enable guessing all the key bit values
stored in lab5aux.o. Please do not try to reverse engineer the key bit values.
Please observe that you will have to find an optimal number of samples - too
few will incur errors, too many may cause a crash.

Upload both the code and your guess of the key (binary, decimal, or hex).

## Franco's Notes
The file lab5aux.o is an ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV)
object file. This file has functions that enable building
iterTime<sub>i,k,b</sub> and spyTime<sub>i,k</sub> tables. The number of input
samples these tables are built for is given by the user as will be shown
below. The spyTime tables are built from  a fixed key of 32 bits. Access to
these tables is through the following functions:

* getIterationTime(i,k,b): Time it takes for the k<sup>th</sup> iteration of
  the exponentiation algorithm on input sample i if the k<sup>th</sup> exponent
  bit (keybit) is b.
* getSpyTime(i,k): Time it takes for the victim's code to complete iterations 0
  through k on the i<sup>th</sup> input sample as observed by the spy thread.

## Key Calculation
To calculate the key, I implemented professor Franco's variance calculation
using the `iterTime(i,k,b)` and `spyTime(i,k)` functions.

My program calculates the absolute difference of (iterTime - spyTime) for the
matrix generated from number of samples times the number of bits in the key
in the case of a 0 key bit and 1 key bit.

```c
double sampleZeroTimeDiffs[si][sk];
double sampleOneTimeDiffs[si][sk];

for (i = 0; i < si; i++) // number of samples
{
    int spyTime = 0;

    // for this sample, get iteration time for each bit in key
    for (k = 0; k < sk; k++)
    {
        spyTime = getSpyTime(i, k);
        sampleZeroTimeDiffs[i][k] = abs(getIterationTime(i, k, 0) \
                                        - spyTime);
        sampleOneTimeDiffs[i][k] = abs(getIterationTime(i, k, 1) \
                                       - spyTime);
    }
}
```

Then I calculate the average time difference for all samples to their
respective bit.

```c
double zeroTimeSums[sk];
double oneTimeSums[sk];

// for each bit, calculate average time of samples for that bit
for (k = 0; k < sk; k++)
{
    for (i = 0; i < si; i++) // number of samples
    {
        zeroTimeSums[k] += sampleZeroTimeDiffs[i][k];
        oneTimeSums[k] += sampleOneTimeDiffs[i][k];
    }

    // average time for kth bit across all samples
    zeroTimeAverages[k] = zeroTimeSums[k] / si;
    oneTimeAverages[k] = oneTimeSums[k] / si;
}
```

I then take these averages to calculate the variance across each bit for each
case (0 or 1).

```c
int keyBits[sk];

for (k = 0; k < sk; k++) // for each bit, calculate variance
{
    double zeroSum = 0.0;
    double oneSum = 0.0;
    for (i = 0; i < si; i++) // number of samples
    {
        zeroSum += pow(sampleZeroTimeDiffs[i][k] \
                       - zeroTimeAverages[k], 2.0);
        oneSum += pow(sampleOneTimeDiffs[i][k] \
                      - oneTimeAverages[k], 2.0);
    }

    double zeroBitVariance = zeroSum / (si - 1);
    double oneBitVariance = oneSum / (si - 1);


    if (zeroBitVariance < oneBitVariance)
    {
        keyBits[k] = 0;
    }
    else
    {
        keyBits[k] = 1;
    }
}
```

If the zero bit variance is larger than the one bit variance then it can be
assumed that bit is a one, else it is a zero bit.

## Key Guess
The largest number of samples I could run my program with without a
segmentation fault was 16350. From this, my program was able to calculate the
key to be 0xBA054D97.

```
Number of keybits: 32   Number of samples: 16350
 0th bit: 0=46.19897    1=1.99528
 1th bit: 0=104.64041   1=37.10405
 2th bit: 0=137.60461   1=72.47887
 3th bit: 0=106.64859   1=172.70800
 4th bit: 0=207.02818   1=141.85919
 5th bit: 0=175.29801   1=241.88542
 6th bit: 0=209.66911   1=277.28548
 7th bit: 0=314.45474   1=244.52552
 8th bit: 0=344.97364   1=281.28785
 9th bit: 0=314.25353   1=385.30338
10th bit: 0=422.30049   1=351.32765
11th bit: 0=455.21501   1=387.27932
12th bit: 0=424.42316   1=492.19599
13th bit: 0=461.33199   1=524.17315
14th bit: 0=562.21598   1=493.10869
15th bit: 0=531.62950   1=595.09891
16th bit: 0=636.83832   1=568.64992
17th bit: 0=604.81881   1=670.93145
18th bit: 0=707.00397   1=642.16079
19th bit: 0=676.46706   1=743.41342
20th bit: 0=712.41474   1=775.85012
21th bit: 0=749.62149   1=816.36649
22th bit: 0=782.52382   1=842.86013
23th bit: 0=814.46743   1=881.56890
24th bit: 0=846.94150   1=914.82877
25th bit: 0=954.36961   1=886.19363
26th bit: 0=922.08364   1=992.77621
27th bit: 0=1023.27121  1=959.55920
28th bit: 0=1052.15172  1=993.10783
29th bit: 0=1094.74505  1=1026.41770
30th bit: 0=1135.08536  1=2957.80325
31th bit: 0=1167.93196  1=1099.19038

Binary Key: 0b10111010000001010100110110010111
Hex Key: 0xBA054D97
```

