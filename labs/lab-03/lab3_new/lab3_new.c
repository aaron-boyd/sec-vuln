#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

void show_logs(char *cmd, char *logfile)
{
    char buffer[100];
    sprintf(buffer, "/var/log/%s", logfile);
    execl(cmd, cmd, buffer, NULL);
}

int main(int argc, char **argv)
{
    printf("Display the log file\n");

    if (argc < 2)
    {
        printf("Usage: %s <log-file>\n\n", argv[0]);
        exit(1);
    }

    show_logs("/usr/bin/tail", argv[1]);
}
