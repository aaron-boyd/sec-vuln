# Security Vulnerability Assessment - Lab 1
Aaron Boyd

## Assignment
You must calculate the risk ratings (critical/high/medium/low/note) for each
upgrade/no-upgrade option. You are only able to upgrade one software package
with the project, so you don't need to perform all permutations. You need to
make a recommendation, for each of the two threat adversary scenarios, as to
which one of the packages should be upgraded and which version it should be
upgraded to (for a tie-breaker, favor the newer version of the software). Use
the OWASP Risk Rating Methodology and Slides as discussed in class, and we will
be making one simplification to the description, and that is that all impact
factors will contribute to a single impact metric, rather than the separate
"technical" and "business" impacts that are discussed in the external link.
Write a report and submit it via Canvas on or before the specified due date
(OK if its late, up to a point). See this link for submission instructions. If
you write any supporting programs or spreadsheets, upload those as well. Any
and all supporting code/notes/documentation will assist in grading if your
conclusions/results differ from mine.

## [OWASP Risk Rating Methodology](http://gauss.ececs.uc.edu/Courses/c5156/labs/risk.pdf)

* Likelihood = average of all likelihood factors

* Impact = average of all impact factors

* Risk = Likelihood * Impact

### Likelihood and Impact Levels
| Score    | Rating |
| -------- | ------ |
| 0 to < 3 | Low    |
| 3 to < 6 | Medium |
| 6 to 9   | High   |

### Risk Severity Table
![Risk Severity Table](./images/severity-table.png "Risk Severity Table")

## Adobe Reader
### Scenario 1 Risk
| Software         | Cost | Impact         | Likelihood     | Risk (Likelihood * Impact) |
| ---------------- | ---- | -------------- | -------------- | -------------------------- |
| Adobe Reader 8   | 0    | 4.000 (Medium) | 6.581 (High)   | 26.324 (High)              |
| Adobe Reader 9.3 | 5    | 5.000 (Medium) | 6.467 (High)   | 32.333 (High)              |
| Adobe Reader 9.4 | 5    | 5.000 (Medium) | 5.838 (Medium) | 29.190 (Medium)            |
| Adobe Reader 10  | 8    | 5.600 (Medium) | 5.790 (Medium) | 32.427 (Medium)            |
| Adobe Reader 11  | 9    | 5.800 (Medium) | 5.762 (Medium) | 33.419 (Medium)            |

### Scenario 2 Risk
| Software         | Cost | Impact         | Likelihood   | Risk (Likelihood * Impact) |
| ---------------- | ---- | -------------- | ------------ | -------------------------- |
| Adobe Reader 8   | 0    | 2.600 (Low)    | 7.152 (High) | 18.596 (Medium)            |
| Adobe Reader 9.3 | 5    | 3.600 (Medium) | 7.038 (High) | 25.337 (High)              |
| Adobe Reader 9.4 | 5    | 3.600 (Medium) | 6.410 (High) | 23.074 (High)              |
| Adobe Reader 10  | 8    | 4.200 (Medium) | 6.362 (High) | 26.720 (High)              |
| Adobe Reader 11  | 9    | 4.400 (Medium) | 6.333 (High) | 27.867 (High)              |

### Overall Risk
| Software         | Scenario 1 | Scenario 2 | Total Risk |
| ---------------- | ---------- | ---------- | ---------- |
| Adobe Reader 8   | High       | Medium     | 44.920     |
| Adobe Reader 9.4 | Medium     | High       | 52.265     |
| Adobe Reader 9.3 | High       | High       | 57.670     |
| Adobe Reader 10  | Medium     | High       | 59.147     |
| Adobe Reader 11  | Medium     | High       | 61.286     |

### Recommendation
Based on my calculations, I recommend the company to not upgrade their version
of Adobe Reader. The above *Overall Risk* section shows that all versions of
the software have a severity of medium/high or above. Using the numerical risk
total from both scenarios, it is clear that Adobe Reader 8 poses the least risk
to the company.

---

## Microsoft Office
### Scenario 1 Risk
| Software              | Cost | Impact         | Likelihood   | Risk (Likelihood * Impact) |
| --------------------- | ---- | -------------- | ------------ | -------------------------- |
| Microsoft Office XP   | 0    | 4.000 (Medium) | 6.500 (High) | 26.000 (High)              |
| Microsoft Office 2003 | 1    | 4.200 (Medium) | 6.343 (High) | 26.640 (High)              |
| Microsoft Office 2007 | 5    | 5.000 (Medium) | 6.257 (High) | 31.286 (High)              |
| Microsoft Office 2010 | 6    | 5.200 (Medium) | 6.100 (High) | 31.720 (High)              |

### Scenario 2 Risk
| Software              | Cost | Impact         | Likelihood   | Risk (Likelihood * Impact) |
| --------------------- | ---- | -------------- | ------------ | -------------------------- |
| Microsoft Office XP   | 0    | 2.600 (Low)    | 7.071 (High) | 26.324 (Medium)            |
| Microsoft Office 2003 | 1    | 2.800 (Low)    | 6.914 (High) | 32.333 (Medium)            |
| Microsoft Office 2007 | 5    | 3.600 (Medium) | 6.829 (High) | 29.190 (High)              |
| Microsoft Office 2010 | 6    | 3.800 (Medium) | 6.671 (High) | 32.427 (High)              |

### Overall Risk
| Software              | Scenario 1 | Scenario 2 | Total Risk |
| --------------------- | ---------- | ---------- | ---------- |
| Microsoft Office XP   | High       | Medium     | 44.386     |
| Microsoft Office 2003 | High       | Medium     | 46.000     |
| Microsoft Office 2007 | High       | High       | 55.869     |
| Microsoft Office 2010 | High       | High       | 57.071     |

### Recommendation
Based on my calculations, I recommend the company to not upgrade their version
of Microsoft Office. The above *Overall Risk* section shows that all versions
of the software have a severity of medium/high or above. Using the numerical
risk total from both scenarios, it is clear that Microsoft Office XP poses the
least risk to the company.
