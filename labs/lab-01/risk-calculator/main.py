from enum import IntEnum


class Rating(IntEnum):
    LOW = 1,
    MEDIUM = 2,
    HIGH = 3


class Risk(IntEnum):
    NOTE = 1,
    LOW = 2,
    MEDIUM = 3,
    HIGH = 4,
    CRITICAL = 5


class Rank(IntEnum):
    MANUAL = 0,
    LOW = 1,
    AVERAGE = 3,
    NORMAL = 5,
    GOOD = 6,
    GREAT = 7,
    EXCELLENT = 9


class Adobe(IntEnum):
    v8 = 8,
    v93 = 93,
    v94 = 94,
    v10 = 10,
    v11 = 11


class MicrosoftOffice(IntEnum):
    oXP = 1,
    o2003 = 2,
    o2007 = 3,
    o2010 = 4


# Scenario 1 Likelihoods
s1_likelihood = {
    'skill': 9,
    'motive': 9,
    'opportunity': 4,
    'size': 3,
    'discovery': 9,
    'awareness': 6
}

# Scenario 1 Impacts
s1_impact = {
    'confidentiality': 9,
    'integrity': 2,
    'availability': 3,
    'financial': 6
}

# Scenario 2 Likelihoods
s2_likelihood = {
    'skill': 7,
    'motive': 9,
    'opportunity': 7,
    'size': 6,
    'discovery': 9,
    'awareness': 6
}

# Scenario 2 Impacts
s2_impact = {
    'confidentiality': 6,
    'integrity': 3,
    'availability': 1,
    'financial': 3
}
# Upgrade costs for each software version
costs = {
    Adobe.v8: 0,
    Adobe.v93: 5,
    Adobe.v94: 5,
    Adobe.v10: 8,
    Adobe.v11: 9,
    MicrosoftOffice.oXP: 0,
    MicrosoftOffice.o2003: 1,
    MicrosoftOffice.o2007: 5,
    MicrosoftOffice.o2010: 6
}

# Adobe Vulnerabilities
adobe_vulnerabilities = {
    'adobe_collectemailinfo': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8]
    },
    'adobe_cooltype_sing': {
        'rank': Rank.GREAT,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_flashplayer_button': {
        'rank': Rank.NORMAL,
        'versions': [Adobe.v8, Adobe.v93, Adobe.v94]
    },
    'adobe_flashplayer_newfunction': {
        'rank': Rank.NORMAL,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_flatdecode_predictor02': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_geticon': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_jbig2decode': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_libtiff': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_media_newplayer': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_pdf_embedded_exe_nojs': {
        'rank': Rank.EXCELLENT,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_pdf_embedded_exe': {
        'rank': Rank.EXCELLENT,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_reader_u3d': {
        'rank': Rank.AVERAGE,
        'versions': [Adobe.v8, Adobe.v93, Adobe.v94, Adobe.v10]
    },
    'adobe_toolbutton': {
        'rank': Rank.NORMAL,
        'versions': [Adobe.v8, Adobe.v93, Adobe.v94, Adobe.v10, Adobe.v11]
    },
    'adobe_u3d_meshdecl': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8, Adobe.v93]
    },
    'adobe_utilprintf': {
        'rank': Rank.GOOD,
        'versions': [Adobe.v8]
    }
}

# Microsoft Office Vulnerabilities
ms_office_vulnerabilities = {
    'ms09_067_excel_featheader': {
        'rank': Rank.GOOD,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007]
    },
    'ms10_004_textbytesatom': {
        'rank': Rank.GOOD,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003]
    },
    'ms10_038_excel_obj_bof': {
        'rank': Rank.NORMAL,
        'versions': [MicrosoftOffice.oXP]
    },
    'ms10_087_rtf_pfragments_bof': {
        'rank': Rank.GREAT,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007, MicrosoftOffice.o2010]
    },
    'ms11_021_xlb_bof': {
        'rank': Rank.NORMAL,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007]
    },
    'ms12_005': {
        'rank': Rank.EXCELLENT,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007, MicrosoftOffice.o2010]
    },
    'ms12_027_mscomctl_bof': {
        'rank': Rank.AVERAGE,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007, MicrosoftOffice.o2010]
    },
    'ms14_017_rtf': {
        'rank': Rank.NORMAL,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007, MicrosoftOffice.o2010]
    },
    'mswin_tiff_overflow': {
        'rank': Rank.AVERAGE,
        'versions': [MicrosoftOffice.oXP, MicrosoftOffice.o2003, MicrosoftOffice.o2007, MicrosoftOffice.o2010]
    },
    'visio_dxf_bof': {
        'rank': Rank.GOOD,
        'versions': [MicrosoftOffice.oXP]
    }
}


def get_level_rating(level):
    """Translate risk level to rating

    :param level: level of risk
    :return: Rating
    """
    if 6.0 <= level:
        return Rating.HIGH
    elif 3 <= level < 6:
        return Rating.MEDIUM
    elif 0 <= level < 3:
        return Rating.LOW


def get_severity(impact, likelihood):
    """Get severity given impact and likelihood factors

    :param impact: impact factor
    :param likelihood: likelihood factor
    :return: Severity of the Risk
    """
    risk_matrix = [[Risk.NOTE, Risk.LOW, Risk.MEDIUM],
                   [Risk.LOW, Risk.MEDIUM, Risk.HIGH],
                   [Risk.MEDIUM, Risk.HIGH, Risk.CRITICAL]]

    return risk_matrix[impact.value - 1][likelihood.value - 1]


def get_exploitability_rating(vulnerabilities, version):
    """Calculate likelihood of a software version being exploited

    :param vulnerabilities: list of vulnerabilities for software package
    :param version: version of software
    :return: average of exploit likelihoods
    """
    ratings = []
    for exploit, info in vulnerabilities.items():
        if version in info['versions']:
            rating = get_level_rating(info['rank'])
            ratings.append(rating)
        else:
            ratings.append(Rank.MANUAL)

    return sum(ratings) / len(ratings)


def calculate_scenario_risk(likelihood, impact, version, vulnerabilities):
    """Calculate risk for a specific version of software in a given scenario

    :param likelihood: scenario likelihood values
    :param impact: scenario impact values
    :param version: version of software
    :param vulnerabilities: list of vulnerabilities for that software package (Adobe, MS Office)
    :return: avg_likelihood, avg_impact
    """
    upgrade_impact = costs[version]
    avg_impact = sum(impact, upgrade_impact) / (len(impact) + 1)

    exploit_likelihood = get_exploitability_rating(vulnerabilities, version)
    avg_likelihood = sum(likelihood, exploit_likelihood) / (len(likelihood) + 1)

    return avg_likelihood, avg_impact


def main():
    software_packages = {
        'Adobe': (Adobe, adobe_vulnerabilities),
        'Microsoft Office': (MicrosoftOffice, ms_office_vulnerabilities),
    }

    for software_package, versions_and_vulnerabilites in software_packages.items():
        risks = []
        versions, vulnerabilities = versions_and_vulnerabilites
        for software_version in versions:
            software_name = software_version.name[1:]
            print(f'{software_package} {software_name}')

            s1_avg_likelihood, s1_avg_impact = calculate_scenario_risk(s1_likelihood.values(),
                                                                       s1_impact.values(),
                                                                       software_version,
                                                                       vulnerabilities)
            s1_likelihood_rating = get_level_rating(s1_avg_likelihood)
            s1_impact_rating = get_level_rating(s1_avg_impact)
            s1_risk = s1_avg_likelihood * s1_avg_impact
            s1_severity = get_severity(s1_impact_rating, s1_likelihood_rating)

            s2_avg_likelihood, s2_avg_impact = calculate_scenario_risk(s2_likelihood.values(),
                                                                       s2_impact.values(),
                                                                       software_version,
                                                                       vulnerabilities)
            s2_likelihood_rating = get_level_rating(s2_avg_likelihood)
            s2_impact_rating = get_level_rating(s2_avg_impact)
            s2_risk = s2_avg_likelihood * s2_avg_impact
            s2_severity = get_severity(s2_impact_rating, s2_likelihood_rating)
            risks.append(f"{software_name} | {s1_severity.name} ({s1_risk:0.3f}) | {s2_severity.name} ({s2_risk:0.3f}) | {s1_risk+s2_risk:0.3f}")

            print('Scenario 1:')
            print(f'\tLikelihood: {s1_likelihood_rating.name} ({s1_avg_likelihood:0.3f})')
            print(f'\tImpact: {s1_impact_rating.name} ({s1_avg_impact:0.3f})')
            print(f'\tRisk: {s1_risk:0.3f}')
            print(f'\tSeverity: {s1_severity.name}\n')
            print('Scenario 2:')
            print(f'\tLikelihood: {s2_likelihood_rating.name} ({s2_avg_likelihood:0.3f})')
            print(f'\tImpact: {s2_impact_rating.name} ({s2_avg_impact:0.3f})')
            print(f'\tRisk: {s2_risk:0.3f}')
            print(f'\tSeverity: {s2_severity.name}\n')

            if s1_risk > s2_risk:
                print(f'Scenario 1 is a bigger risk to {software_package} {software_name}.')
            elif s1_risk < s2_risk:
                print(f'Scenario 2 is a bigger risk to {software_package} {software_name}.')
            else:
                print(f'Scenario 1 and 2 are equal risk to {software_package} {software_name}.')

            print('-----------------------------------------------------------')
        for risk in risks:
            print(f"| {risk} |")
        print('')


if __name__ == '__main__':
    main()
