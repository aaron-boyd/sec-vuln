# Security Vulnerability Assessment - Lab 4
Aaron Boyd

## Assignment
Determine for what inputs this
[ELF-64](http://gauss.ececs.uc.edu/Courses/c5156/labs/lab4) bit binary will
rewrite stack addresses and what will be the result if it does. Modify the code
keeping functionality the same but removing the vulnerability (in place of the
effects of the vulnerability, print "vulnerability removed" to the console).
Submit the modified binary plus the result of your examination of it.

## Notes
To reverse engineer this program I used the IDA disassembler. I used the "IDA
Graph View" to trace through the assembly and convert it back to source.

I noticed that the program takes in `argv[1]` and converts it to a number using
the `atoi(char*)` function and stores the value in the variable `thevariable`
(see Figure 1). The program then references this global variable multiple times
in different functions.

<center>
  <img src="./images/atoi.png" alt="Figure 1: Converting argv[1]"/>
  <div>Figure 1: Converting argv[1] to a number</div>
</center>


The variable `thevariable` is then checked to see if it is greater than 0x31.
If it is greater than 0x31 the call to `sleep()` can be skipped (see Figure 2)
and the program will go to the check of `argc`.

<center>
  <img src="./images/skip_sleep.png" alt="Skip the sleep function"/>
  <div>Figure 2: Skip the sleep function'</div>
</center>

In order to get the program to output "this is the toughest of them all...",
`thevariable` needs to be greater than 0x1d (29) (see Figure 3).

<center>
  <img src="./images/this_is_the_toughest_of_them_all.png" alt="Printing 'this is the toughest of them all...'"/>
  <div>Figure 3: Printing 'this is the toughest of them all...'</div>
</center>

The program then checks to see if the argument count (`argc`) is greater than
2. If so, it will set the global variable `printstack` to 1 (see Figure 4).
The program then calls the `h()` function.

<center>
  <img src="./images/call_h.png" alt="Set printstack and call h() function"/>
  <div>Figure 4: Set printstack and call h() function</div>
</center>

Inside the `h()` function, a stack of size `0x10` is allocated and a local
variable is set to 0x123. Then the `f(int)` function is called with the value
0x123 passed in (see Figure 5).

<center>
  <img src="./images/call_f.png" alt="Call f() function"/>
  <div>Figure 5: Call h() function</div>
</center>

Inside the `f(int)` function a stack of size 0x10 is allocated. The int passed
into f is stored in a local variable (I call it `localNum`). This is the
function that contains the vulnerability. In this function the value of
`thevariable` is checked. If it is greater than 0x31 (49) the address of the
`g()` function is moved into the address of `localNum+0x30` (see Figure 6).

<center>
  <img src="./images/vulnerability.png" alt="Vulnerability"/>
  <div>Figure 6: Vulnerability</div>
</center>

The state of the stack at this time is depicted below:

```
       ------------------------
0x0000 |                      |
       ------------------------
0x0008 |                      |
       ------------------------
0x0010 |  0x00                | <- rsp
       ------------------------
0x0018 |  f.localNum (0x123)  |
       ------------------------
0x0020 |  h.rbp               | <- rbp
       ------------------------
0x0028 |  h.return            |
       ------------------------
0x0030 |  0x00                |
       ------------------------
0x0038 |  h.num(0x123)        |
       ------------------------
0x0040 |  main.rbp            |
       ------------------------
0x0048 |  main.return         |
       ------------------------
```

When 0x30 is added to the address of `f.localNum`, in this example, 0x18 the
return address of main's stack-frame is overwritten with the address of `g()`
because 0x18 + 0x30 = 0x48.

```
       ------------------------
0x0000 |                      |
       ------------------------
0x0008 |                      |
       ------------------------
0x0010 |  0x00                | <- rsp
       ------------------------
0x0018 |  f.localNum (0x123)  |
       ------------------------
0x0020 |  h.rbp               | <- rbp
       ------------------------
0x0028 |  h.return            |
       ------------------------
0x0030 |  0x00                |
       ------------------------
0x0038 |  h.num(0x123)        |
       ------------------------
0x0040 |  main.rbp            |
       ------------------------
0x0048 |  address of g        |
       ------------------------
```

Since the return address of main's stack-frame is overwritten, when main goes
to return it will go to `g()` instead of `__lib_c_start_main`. By looking at
the disassembly of `g()`, it can be seen that this function executes a shell
(see Figure 7).

<center>
  <img src="./images/g.png" alt="g() function opening a shell"/>
  <div>Figure 7: g function opening a shell</div>
</center>

In order to exploit the vulnerability in this program and skip the `sleep()`
function `argv[1]` needs to be greater than 49 (see Figure 8).

<center>
  <img src="./images/exploit_no_sleep.png" alt="Exploiting vulnerability with no sleep() function"/>
  <div>Figure 8: Exploiting vulnerability with no sleep() function</div>
</center>


If the `printStack()` function wants to be skipped `argc` needs to be less
than 2 (see Figure 9).

<center>
  <img src="./images/exploit_with_sleep.png" alt="Exploiting vulnerability with sleep() function"/>
  <div>Figure 9: Exploiting vulnerability with sleep() function</div>
</center>


If `thevariable` is greater than 0x1d but less than 0x27 (39) the program will
print out "Here is some help:" and call `printStack()` but the vulnerability
will not be exploited (see Figure 10 and 11).


<center>
  <img src="./images/here_is_some_help_code.png" alt="Printing out 'Here is some help:' and calling printStack() in IDA"/>
  <div>Figure 10: Printing out 'Here is some help:' and calling printStack() in
  IDA</div>
</center>

<center>
  <img src="./images/here_is_some_help.png" alt="Printing out 'Here is some help:' and calling printStack()"/>
  <div>Figure 11: Printing out 'Here is some help:' and calling printStack(
  )</div>
</center>

## Original Source
Below is what I interpreted the assembly to look like in C.

```c
int h()
{
    return f(0x123);
}

int f(int num)
{
    int localNum = num;

    if (printStack != 0)
    {
        printStack(&localNum + 0x30);
    }

    if (thevariable > 0x31 ) // '1'
    {
         // overwrites the return address of main
        *(&localNum + 0x30) = &g;
    }

    if (printStack != 0)
    {
        printStack(&localNum + 0x30);
    }

    return &localNum;
}

void g()
{
    puts("Now you did it - executing shellcode - this is bad!");
    execvp("/bin/sh", NULL);
}

static int thevariable = 0;
static int printstack = 0;

int main(int argc, char** argv) {
    if (argc < 1)
    {
        puts("Brrr.. you are cold, but have a nice day!");
        exit(0);
    }

    thevariable = atoi(argv[1]);

    if (thevariable <= 0x31 )
    {
        printf("Running normally...");
        printf("still thinking...");
        sleep(2);

        if (thevariable > 0x13)
        {
            if(thevariable > 0x1d)
            {
                printf("this is the toughest of them all...");
            }
            else
            {
                printf("this is a real toughie...");
            }
        }
        else
        {
            printf("this is a toughie...");
        }
        sleep(2);
        putchar('\n');
    }

    sleep(2);

    if (argc >= 0x2)
    {
        printstack = 0x1;
    }
    else
    {
        printstack = 0x0;
    }

    h();

    if (thevariable > 0x9)
    {
        if (thevariable > 0x13)
        {
            if (thevariable > 0x1d)
            {
                if (thevariable > 0x27)
                {
                    puts("Here is some help:");
                    printStack(&argv)
                }
            }
            else
            {
                puts("Getting hot");
            }
        }
        else
        {
            puts("Getting warmer");
        }
    }
    else
    {
        puts("Getting warm");
    }

    return 0;
}
```

## Vulnerability Fix
In order to remove the vulnerability from the program without having the
source I nop-ed out the section of code that overwrites the return address. It
can be seen in Figure 12 that this code ranges from 0x8d0 to 0x8e2.

<center>
  <img src="./images/code_to_nop.png" alt="Code to NOP in IDA"/>
  <div>Figure 12: Code to NOP in IDA</div>
</center>

This can be easily done using `xxd`.

```
$ xxd lab4_old > lab4_old.hex
```

In a text editor the values can be changed to the `nop` instruction 0x90. This
essentially creates a "nop-sled" so when the program gets here it will just
"slide down" the nops until it hits normal code again.

```
000008d0: 488d 45f8 4883 c030 488d 159b ffff ff48  H.E.H..0H......H
000008e0: 8910 0fb6 0533 1720 0084 c074 1048 8d45  .....3. ...t.H.E
```

changes to:

```
000008d0: 9090 9090 9090 9090 9090 9090 9090 9090  ................
000008e0: 9090 0fb6 0533 1720 0084 c074 1048 8d45  .....3. ...t.H.E
```

The file `lab4_old.hex` can be converted back to a running binary with:

```
$ xxd -r lab4_old.hex lab4_new
```

Now when `lab4_new` is run with input that should exploit the vulnerability
the program executes the nops instead of the vulnerable code (see Figure 13).

<center>
  <img src="./images/patched_binary.png" alt="Patched binary with no vulnerability"/>
  <div>Figure 13: Patched binary with no vulnerability</div>
</center>

## Fixed Source Code
To fix the source code I would make the following change:

```c
int f(int num)
{
    int localNum = num;

    if (printStack != 0)
    {
        printStack(&localNum + 0x30);
    }

    if (thevariable > 0x31 ) // '1'
    {
        printf("vulnerability removed\n");
    }

    if (printStack != 0)
    {
        printStack(&localNum + 0x30);
    }

    return &localNum;
}
```

